use std::collections::HashMap;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn main() {
    let f = File::open("input").expect("file not found");
    let file = BufReader::new(&f);

    let mut contents: Vec<i64> = Vec::new();

    for line in file.lines() {
        let l = line.unwrap();
        let l = l.replace("+", "");

        contents.push(l.parse::<i64>().unwrap());
    }

    let mut total = 0;
    let mut single_loop_total = 0;
    let dupe;

    let mut frequencies: HashMap<i64, i64> = HashMap::new();

    let mut looped = false;

    'outer: loop {
        for n in contents.clone() {
            match frequencies.get(&total) {
                Some(occurences) => frequencies.insert(total, occurences + 1),
                _ => frequencies.insert(total, 1),
            };

            if frequencies[&total] >= 2 {
                dupe = total;
                println!("dupe: {}", dupe);
                break 'outer;
            }

                total += n;
        }
        if !looped {
            single_loop_total = total;
        }
        
        looped = true;
    }

    println!("single loop total: {}", single_loop_total);
    println!("total: {}", total);
}
